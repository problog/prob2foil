#! /usr/bin/env python
from __future__ import print_function

import glob

for filename in glob.glob('*/modelfile'):
    fold = 0
    file_out = None
    with open(filename) as file_in:
        for line in file_in:
            if line.startswith('%TARGET'):
                fold += 1
                if file_out is not None:
                    file_out.close()
                file_out = open(filename + str(fold), 'w')
                print (line.strip(), file=file_out)
            elif '<-' in line:
                print (line.strip(), file=file_out)
    if file_out is not None:
        file_out.close()
