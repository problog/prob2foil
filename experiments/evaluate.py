#! /usr/bin/env python

from __future__ import print_function
import sys
from problog.logic import Clause, Term, Constant
from problog.program import PrologFile, PrologString
from problog.sdd_formula import SDD
from problog.nnf_formula import NNF
from problog.formula import LogicDAG, DeterministicLogicFormula
from problog.evaluator import SemiringLogProbability
from problog.engine import DefaultEngine, ClauseDB


class FilteredPrologString(PrologString):
    """PrologString where corner case probabilities are simplified, that is,
        facts with probability 1.0 are replaced by deterministic facts
        and facts with probability 0.0 are removed.
    """

    def __init__(self, string):
        PrologString.__init__(self, string)

    def __iter__(self):
        for line in PrologString.__iter__(self):
            if hasattr(line, 'probability') and line.probability is not None:
                if float(line.probability) == 1.0:
                    yield line.withProbability()
                elif float(line.probability) == 0.0:
                    pass
                else:
                    yield line
            else:
                yield line


def main(setting, *targets):
    for target in targets:
        print ('Processing target %s' % target)
        for fold in range(1, 4):
            print ('Processing fold %s' % fold)
            # Initial filenames and target settings
            rule_arity = 2
            rule_file = 'resultssetting%s/%s/modelfile%s' % (setting, target, fold)
            prob_file_train = 'dataset_prob/%s/%s_train%s.pl' % (target, target, fold)
            prob_file_test = 'dataset_prob/%s/%s_test%s.pl' % (target, target, fold)
            score_file_out = 'resultssetting%s/%s/scores%s' % (setting, target, fold)

            # Load the rules from the file with learned rules
            rules = PrologFile(rule_file)

            # Determine the positive examples in the test data
            query = Term(target, *range(0, rule_arity))
            with open(prob_file_test) as f:
                data = f.read() + '\n'
            data = FilteredPrologString(data)
            data_db = DefaultEngine().prepare(data)
            for rule in rules:
                data_db += rule
            query_db = ClauseDB(parent=data_db)
            query_db += Term('query', query)

            # Note: using a DeterministicLogicFormula eliminates negative examples.
            # gp = DefaultEngine().ground_all(query_db, DeterministicLogicFormula())
            gp = DefaultEngine().ground_all(query_db)
            examples = [q.args for q, n in gp.queries()]

            # Compute the probabilities for each rule
            rule_head_correct = query
            rule_head_learned = Term('pf_eval_' + target, *range(0, rule_arity))
            query_head_correct = Term('q1')
            query_head_learned = Term('q2')

            with open(score_file_out, 'w') as file_out:
                for example in examples:
                    query_db = ClauseDB(parent=data_db)
                    query_db += Clause(query_head_correct, rule_head_correct(*example))
                    query_db += Clause(query_head_learned, rule_head_learned(*example))
                    query_db += Term('query', query_head_correct)
                    query_db += Term('query', query_head_learned)
                    gp = DefaultEngine().ground_all(query_db)
                    result = NNF.createFrom(gp).evaluate()
                    c, l = result[query_head_correct], result[query_head_learned]
                    print(c, l, file=file_out)
                    print('.', end='')
                    sys.stdout.flush()
            print()


if __name__ == '__main__':
    main(*sys.argv[1:])