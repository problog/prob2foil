:- use_module(library(problog)).
:- use_module(library(problog_learning)).

main :-
    unix( argv(ARGS)),
    (ARGS = [Data, Rules, Examples] ->
        consult(Data),
        consult(Rules),
        consult(Examples),
        do_learning(10)
    ;
        halt(5)
    ).


