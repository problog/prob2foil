#! /usr/bin/env python


from __future__ import print_function
import sys
import os


def initialize(target, fold):
    # Initialize rules
    rule_file_in = 'resultssetting1/%s/modelfile%s' % (target, fold)
    data_file_in = 'dataset_prob/%s/%s_train%s.pl' % (target, target, fold)
    learn_file_out = 'resultssettingLFE/%s/learn%s.pl' % (target, fold)
    try:
        os.makedirs(os.path.dirname(learn_file_out))
    except OSError:
        pass

    with open(learn_file_out, 'w') as file_out:
        print('% Preamble', file=file_out)
        print(':- use_module(library(problog)).', file=file_out)
        print(':- use_module(library(problog_learning)).', file=file_out)
        print(file=file_out)

        print('% Data', file=file_out)
        with open(data_file_in) as file_in:
            print(file_in.read(), file=file_out)
        print(file=file_out)

        ex = 1
        print('% Examples', file=file_out)
        with open(data_file_in) as file_in:
            for line in file_in:
                if '::' + target in line:
                    prob, example = line.strip().split('::')
                    print('example(%s,pf_eval_%s,%s).' % (ex, example[:-1], prob), file=file_out)
                    ex += 1
        print(file=file_out)

        print ('% Rules', file=file_out)
        with open(rule_file_in) as file_in:
            r = 0
            for line in file_in:
                if not line.startswith('%'):
                    r += 1
                    rule = line.strip().split('::')[1].replace('<-', ':-')
                    print('t(_)::r%s.' % r, file=file_out)
                    print(rule, file=file_out)
            print(file=file_out)

        print ('% Execute', file=file_out)
        print (':- do_learning(10).', file=file_out)


def main(*targets):
    for target in targets:
        for fold in range(1, 4):
            initialize(target, fold)


if __name__ == '__main__':
    main(*sys.argv[1:])