#! /usr/bin/env python

from __future__ import print_function

import glob
from collections import defaultdict

def recall(filename):
    P = 0
    M = 0
    TP = 0.0 
    FP = 0.0
    with open(filename) as f:
        for line in f:
            c, p = map(float,line.strip().split())
            P += c
            M += 1
            TP += min(c, p)
            FP += max(p-c, 0)
        N = M - P
        TN = N - FP
        FN = P - TP

    return TP / (TP + FN)


def compute(setting):
    scores = defaultdict(float)
    print ('Scores for setting %s' % setting)
    for filename in glob.glob('resultssetting%s/*/scores?' % setting):
        try:
            key, fold = filename.split('/')[1:3]
            p = recall(filename)
            scores[key] += p
        except:
            scores[key] = float('nan')

    for key in scores:
        print (key, scores[key]/3)

if __name__ == '__main__':
    for s in ['X', 'Y', 'Yb', '1', 'LFE']:
        compute(s)