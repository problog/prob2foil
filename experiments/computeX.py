#! /usr/bin/env python

from __future__ import print_function
import sys
from problog.logic import Clause, Term, Constant
from problog.program import PrologFile, PrologString
from problog.sdd_formula import SDD
#from problog.nnf_formula import NNF
from problog.formula import LogicDAG, DeterministicLogicFormula
from problog.evaluator import SemiringLogProbability
from problog.engine import DefaultEngine, ClauseDB

class FilteredPrologString(PrologString):

    def __init__(self, string):
        PrologString.__init__(self, string)

    def __iter__(self):
        for line in PrologString.__iter__(self):
            if hasattr(line, 'probability') and line.probability is not None:
                if float(line.probability) == 1.0:
                    yield line.withProbability()
                elif float(line.probability) == 0.0:
                    pass
                else:
                    yield line
            else:
                yield line


def main(*targets):
    for target in targets:
        print ('Processing target %s' % target)
        for fold in range(1, 4):
            print ('Processing fold %s' % fold)
            # Initial filenames and target settings
            rule_functor = target
            rule_arity = 2
            rule_file = 'resultssetting1/%s/modelfile%s' % (target, fold)
            prob_file_train = 'dataset_prob/%s/%s_train%s.pl' % (target, target, fold)
            prob_file_test = 'dataset_prob/%s/%s_test%s.pl' % (target, target, fold)
            rule_file_out = 'resultssettingX/%s/modelfile%s' % (target, fold)

            # Load the rules from the file with learned rules
            rules = PrologFile(rule_file)

            # Determine the positive examples in the training data
            query = Term(rule_functor, *range(0, rule_arity))
            with open(prob_file_train) as f:
                data = f.read() + '\n'
            data = FilteredPrologString(data)
            data_db = DefaultEngine().prepare(data)
            query_db = ClauseDB(parent=data_db)
            query_db += Term('query', query)
            gp = DefaultEngine().ground_all(query_db, DeterministicLogicFormula())
            examples = [q.args for q, n in gp.queries()]

            with open(rule_file_out, 'w') as file_out:
                # Compute the probabilities for each rule
                for i, rule in enumerate(rules):
                    rule_head = rule.head.withProbability()
                    query_head = Term('q')
                    score = 0.0
                    for example in examples:
                        query_db = ClauseDB(parent=data_db)
                        query_db += rule
                        query_db += Clause(query_head, rule_head(*example))
                        query_db += Term('query', query_head)
                        gp = DefaultEngine().ground_all(query_db)
                        score += SDD.createFrom(gp).evaluate()[query_head]
                    score /= len(examples)
                    new_rule = Clause(rule_head.withProbability(Constant(score)), rule.body)

                    print (new_rule)
                    print (str(new_rule) + '.', file=file_out)

if __name__ == '__main__':
    main(*sys.argv[1:])