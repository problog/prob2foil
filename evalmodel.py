from __future__ import print_function
import problog
import sys

def main(rules, data):

    target = None

    model = []
    with open(rules) as frules:
        for line in frules:
            if line.startswith('%TARGET:'):
                target = problog.logic.Term.from_string(line[9:])
            model.append(line)

    with open(data) as fdata:
        for line in fdata:
            model.append(line)

    target_pf = problog.logic.Term('pf_eval_%s' % target.functor)

    model = problog.program.PrologString('\n'.join(model))

    eng = problog.engine.DefaultEngine()
    db = eng.prepare(model)
    examples = eng.query(db, target)

    for ex in examples:
        gp = eng.ground(db, target_pf(*ex), label='query')
        p_predict = problog.get_evaluatable().create_from(gp).evaluate().values()[0]
        gp = eng.ground(db, target(*ex), label='query')
        p_correct = problog.get_evaluatable().create_from(gp).evaluate().values()[0]

        print (p_predict, p_correct)






if __name__ == '__main__':
    main(*sys.argv[1:])

