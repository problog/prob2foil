
base( a(ex) ).
base( b(ex) ).
base( c(ex) ).

base( ok(ex) ).

modes( a(+) ).
modes( b(+) ).
modes( c(+) ).

learn( ok(ex) ).

a(1).
b(1).
0.7::ok(1).

c(2).
0.4::ok(2).

a(3).

b(4).

a(5).
b(5).
c(5).
0.82::ok(5).
