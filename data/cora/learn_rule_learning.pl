base(rule_learning(paper)).

1.0::rule_learning(X) :- paper(X, 'Rule_Learning').
0.0::rule_learning(X) :- paper(X, _), \+ paper(X, 'Rule_Learning').

:- consult(data).
